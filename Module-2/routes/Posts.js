// Import Modules
const express = require('express')

// Our Router
let Posts = express.Router()
let Comments = require('./Comments.js')
// BlogPost Routes

/*
	GET /posts
	Gets all the Blog Posts
*/
Posts.get('/', (req, res)=> {
	let posts = req.store.posts
	res.send(JSON.stringify(posts, null, 4))
})
/*
	POST /posts
	Creates a new Blog Post
*/
Posts.post('/', (req, res)=> {
	let newPost = req.body
	let postID = req.store.posts.length
	req.store.posts.push(newPost)
	res.status(201).send(JSON.stringify({postID: postID}, null, 4))
})
/*
	PUT /posts/:postID
	Updates a Blog Post with ID postID
*/
Posts.put('/:postID', (req, res)=> {
	let id = req.params.postID
	let post = req.store.posts[id]
	req.store.posts[id] = Object.assign(post, req.body)
	res.send(JSON.stringify(req.store.posts[id], null, 4))
})
/*
	DELETE /posts/:postID
	Deletes a Blog Post with ID postID
*/
Posts.delete('/:postID', (req, res)=> {
	let id = req.params.postID
	req.store.posts.splice(id, 1)
	res.status(200).send('Deleted Post')
})

// Comments Routes
Posts.use('/:postID/comments', Comments)

module.exports = Posts