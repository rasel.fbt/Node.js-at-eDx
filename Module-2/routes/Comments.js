// Import Modules
const express = require('express')

// Our Router
let Comments = express.Router({mergeParams: true})

// Comment Routes

/*
	GET /posts/:postID/comments
	Gets all the Comments of a Blog Post
*/
Comments.get('/', (req, res)=> {
	let id = req.params.postID
	let comments = req.store.posts[id].comments
	res.send(JSON.stringify(comments, null, 4))
})
/*
	POST /posts/:postID/comments
	Creates a new Comment in a Blog Post
*/
Comments.post('/', (req, res)=> {
	let newComment = req.body
	let pid = req.params.postID
	let comments = req.store.posts[pid].comments
	let cid = comments.length
	comments.push(newComment)
	res.status(201).send(JSON.stringify({commentID: cid}, null, 4))
})
/*
	PUT /posts/:postID/comments/:commentID
	Updates a Comment with ID commentID of Blog Post postID
*/
Comments.put('/:commentID', (req, res)=> {
	let pid = req.params.postID
	let comments = req.store.posts[pid].comments
	let cid = req.params.commentID
	let comment = comments[cid]
	req.store.posts[pid].comments[cid] = Object.assign(comment, req.body)
	res.send(JSON.stringify(req.store.posts[pid].comments[cid], null, 4))
})
/*
	DELETE /posts/:postID/comments/:commentID
	Deletes a Comment with ID commentID of Blog Post postID
*/
Comments.delete('/:commentID', (req, res)=> {
	let pid = req.params.postID
	let cid = req.params.commentID
	req.store.posts[pid].comments.splice(cid, 1)
	res.send('Deleted Comment')
})


module.exports = Comments
