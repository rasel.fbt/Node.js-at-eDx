// Import Modules
const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')

// Our Routes
const posts = require('./routes/Posts.js')

// Our Data
var Store = require('./data/Store.json')

// Our App Instance
let app = express()

// Setting up Middlewares
app.use(bodyParser.json())
app.use(logger('dev'))
app.use((req, res, next)=> {
	req.store = Store
	next()
})

// Use Routes
app.use('/posts', posts)

// Start our Server
app.listen(3000, ()=> {
	console.log('Listening on port 3000')
})