// Import Modules
const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')
const errorHandler = require('errorhandler')

// Our Routes
const accounts = require('./routes/Accounts.js')

// Our App Instance
let app = express()

// Setting up Middlewares
app.use(logger('dev'))
app.use(bodyParser.json())

// Use Routes
app.use('/accounts', accounts)

// Error Handling
app.use(errorHandler)

// Start our Server
app.listen(3000, ()=> {
	console.log('Listening on port 3000')
})