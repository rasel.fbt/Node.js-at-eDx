// Import Modules
const express = require('express')
const mongoose = require('mongoose')

// Database Location
const url = 'mongodb://localhost:27017/test'

// Our Router
let Accounts = express.Router()

// Account Routes
const Account = mongoose.model('Account', {
    name: String,
    balance: Number
})

/*
	GET /accounts
	Gets all the Accounts
*/
Accounts.get('/', (req, res, next) => {
    mongoose.connect(url, { useNewUrlParser: true })
    Account.find({}, null, { sort: { _id: -1 } }, (error, accounts) => {
        if (error) return next(error)
        res.send(accounts)
    	mongoose.connection.close()
    })
})
/*
	POST /accounts
	Creates a new Account
*/
Accounts.post('/', (req, res, next) => {
	mongoose.connect(url, { useNewUrlParser: true })
    let newAccount = new Account(req.body)
    newAccount.save((error, results) => {
        if (error) return next(error)
        res.send(results)
    	mongoose.connection.close()
    })
})
/*
	PUT /accounts/:id
	Updates a Account with ID id
*/
Accounts.put('/:id', (req, res, next) => {
	mongoose.connect(url, { useNewUrlParser: true })
    Account.findById(req.params.id, (error, account) => {
        if (error) return next(error)
        if (req.body.name) account.name = req.body.name
        if (req.body.balance) account.balance = req.body.balance
        account.save((error, results) => {
            res.send(results)
            mongoose.connection.close()
        })
    })
})
/*
	DELETE /accounts/:id
	Deletes a Account with ID id
*/
Accounts.delete('/:id', (req, res, next) => {
	mongoose.connect(url, { useNewUrlParser: true })
    Account.findById(req.params.id, (error, account) => {
        if (error) return next(error)
        account.remove((error, results) => {
            if (error) return next(error)
            res.send(results)
        	mongoose.connection.close()
        })
    })
})

module.exports = Accounts