/* Node.js Script to Convert CSV into JSON*/

// Importing some modules
const fs = require('fs');
const path = require('path');
var CSVToJSON = require('./lib/csv-to-json.js');

// Our CSV Read Stream
const src = path.join(__dirname, 'customer-data.csv');
const CSVStream = fs.createReadStream(src);

// Read and Parse CSV
CSVStream.on('data', CSVToJSON.ParseStream);

// Write the produced JSON to file
CSVStream.on('end', CSVToJSON.EndStream);