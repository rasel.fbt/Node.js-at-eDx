// Object Generator
function generateObject(arrayOfKeys, valuesWithComma) {
	var valuesArray = valuesWithComma.split(',');
	var o = {};
	for(var k = 0; k < arrayOfKeys.length; k++) {
		o[arrayOfKeys[k]] = valuesArray[k];
	}
	return o;
}

module.exports = {
	GenerateObject : generateObject
};