// Import Utility Functions
const fs = require('fs');
const path = require('path');
var util = require('./util.js');

// Empty Answer : This will be an array of JSON Objects
var OutputJSON = [];

// An Array to keep the Object Keys
var Keys = [];

// Single Row to Object Generator Function
var generateObject = util.GenerateObject;

// Temporary Buffer Variable
var RawData = '';

// Parsing Data
function parseStream(bits) {
	// Check if we already have the Keys set or not
	if(Keys.length) { // We can carry on extracting data
		// Start directly with RawData
		RawData += bits;
		var Rows = RawData.split('\r\n');
		// Generate Objects and add to our Answer JSON
		// Skipping the last row intentionally every time
		for(var j = 0; j < Rows.length-1; j++) {
			var Info = Rows[j];
			OutputJSON.push(generateObject(Keys, Info));
		}
		// Last row may be incomplete. Will be used in next call
		RawData = Rows[Rows.length-1];
	} else { // Let's get the Keys first
		// We might be either continuing an earlier operation from RawData
		// Or it would be an empty string. Either way, we're good this way
		var Chunk = RawData + bits.toString();
		// Check if we have the first row with us in Chunk or not
		var LineBreak = Chunk.indexOf('\r\n');
		if(LineBreak !== -1) { // We have first row in this Chunk
			var Headers = Chunk.substring(0, LineBreak);
			// Set our Keys for JSON Object
			Keys = Headers.split(',');
			// Don't waste the remaining data! 
			// CRLF takes 2 index places, so starting from + 2
			RawData = Chunk.substring(LineBreak + 2, Chunk.length);
		} else { // We don't have first row, so keep current Chunk intact
			RawData = Chunk;
		}
	}
}

// Finalising JSON and Writing to File
function endStream() {
	// In given CSV, there was a CRLF at the end, so this check isn't needed. Added for flexibility.
	if(RawData.length){
		var Info = generateObject(Keys, RawData);
		OutputJSON.push(Info);
	}
	console.log(OutputJSON.length + ' Records Were Extracted');
	var dest = path.join(__dirname, 'customer-data.json');
	var data = JSON.stringify(OutputJSON, null, '\t');
	fs.writeFile(dest, data, 'utf8', (err) => {
		if(err) {
			console.error(err.message);
		}
		console.log('Records Saved Successfully!');
	});
}

module.exports = {
	ParseStream : parseStream,
	EndStream : endStream
}