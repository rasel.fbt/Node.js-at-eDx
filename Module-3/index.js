// Import Our Modules and Data
const async = require('async')
const customers = require('./data/m3-customer-data.json')
const customerAddresses = require('./data/m3-customer-address-data.json')
const errorHandler = require('./util/errorHandler.js')
let writeToDatabase = require('./util/insertDocuments.js')
// Handle CLI Arguments
let Arguments = process.argv.slice(2)
let NumberOfObjectsToProcessInOneQuery = null;
if(Arguments.length == 1) { /*There should only be one argument*/
	let batchSize = parseInt(Arguments[0])
	if(isNaN(batchSize) === false) { /*It should be a number*/
		let remainder = 1000 % batchSize
		if(remainder === 0) { /*It should evenly divide the set*/
			NumberOfObjectsToProcessInOneQuery = batchSize
		} else {
			errorHandler(3)
		}
	} else {
		errorHandler(2)
	}
} else {
	errorHandler(1)
}

// Create Task Functions
let NumberOfQueries = 1000 / NumberOfObjectsToProcessInOneQuery
let Tasks = []
for(let i = 0; i < NumberOfQueries; i++) {
	let Start = i * NumberOfObjectsToProcessInOneQuery
	let End = Start+NumberOfObjectsToProcessInOneQuery
	Tasks.push(function (Callback) {
		let _Start = Start
		let _End = End
		let Documents = []
		console.log(`Processing Records from ${_Start+1} to ${_End}`)
		for(let j = _Start; j < _End; j++) {
			let Document = {}
			Object.assign(Document, customers[j], customerAddresses[j])
			Documents.push(Document)
		}
		writeToDatabase(Documents, _Start, _End, Callback)
	})
}

async.parallel(Tasks, function(err, res) {
	console.log(res)
})