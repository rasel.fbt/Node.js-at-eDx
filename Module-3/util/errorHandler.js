// Handle CLI Error
const errors = {
	"1": {
		"message": "Arguments do not match the format."
	},
	"2": {
		"message": "Argument supplied must be a number."
	},
	"3": {
		"message": "BATCHSIZE must evenly divide total records (1000)."
	},
}

const usageText = "Usage :\n\n\tnode index.js BATCHSIZE\n\nwhere BATCHSIZE must be a number which evenly divides the total number of records (1000)"

module.exports = function(errorCode) {
	console.log(errors[errorCode].message)
	console.log(usageText)
	process.exit(1)
}