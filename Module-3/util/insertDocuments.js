// Import MongoDB Driver
const mongodb = require('mongodb')
const url = 'mongodb://localhost:27017/edx-course-db'

module.exports = function(docs, start, end, cb) {
    mongodb.MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
        if (err)
            cb(err, null)
        let db = client.db('edx-course-db')
        db.collection('customers').insert(docs, function(err, res) {
        	client.close()
            if (err)
                cb(err, null)
            console.log(`Processed Records from ${start+1} to ${end}`)
        })
    })
}